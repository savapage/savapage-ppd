::------------------------------------------------------------
:: Building SavaPage Printer Driver OEM UI's
::
:: SPDX-FileCopyrightText: (c) 2014 Datraverse B.V. <info@datraverse.com>
:: SPDX-License-Identifier: AGPL-3.0-or-later
::
::------------------------------------------------------------
@call buildchk_win7.cmd x86
@call buildchk_win7.cmd x64
@call buildchk_win7.cmd ia64
:: end-of-file